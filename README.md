# Dockerized MouseTrap

This project contains a docker configuration for running all services of MouseTrap.
With this configuration we can redeploy the services with a single command when a
change is done. Also, it is not necessary to install all the dependencies on the host
machine.

## Getting Started

Clone the repo or download it.

## Requirements

Make sure that you have docker and docker-compose. If you don't have, run the install
script `install-docker.sh`. *Reboot* after installing.

## Deployment

To deploy the services run:

```
docker-compose up -d --build

```

## Cleanup

If you need to cleanup docker images, run:

```bash
docker-compose stop # stop containers
docker-compose rm # remove containers
docker rmi $(docker images -q) -f # WARNING: this removes all your images

```

or

```bash
sudo docker-gc

```

## Issue Reporting

If you have found a bug or if you have a feature request, please report them at this repository issues section. Please do not report security vulnerabilities on the public issues section, instead send an email
to up201200272 [at] fc.up.pt

## Author

[José C. Paiva](#)

## License

This project is licensed under the MIT license. See the [LICENSE](LICENSE.txt) file for more info.
