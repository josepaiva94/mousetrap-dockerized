# Ask for the user password
# Script only works if sudo caches the password for a few minutes
sudo true

# Alternatively you can use the official docker install script
wget -qO- https://get.docker.com/ | sh

sudo usermod -aG docker "`whoami`"

# Install docker-compose
COMPOSE_VERSION=`git ls-remote https://github.com/docker/compose | grep refs/tags | grep -oP "[0-9]+\.[0-9][0-9]+\.[0-9]+$" | tail -n 1`
sudo sh -c "curl -L https://github.com/docker/compose/releases/download/${COMPOSE_VERSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
sudo chmod +x /usr/local/bin/docker-compose
sudo sh -c "curl -L https://raw.githubusercontent.com/docker/compose/${COMPOSE_VERSION}/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose"

# Install docker-cleanup command
sudo cp docker-gc /usr/local/bin/docker-gc
sudo chmod +x /usr/local/bin/docker-gc

